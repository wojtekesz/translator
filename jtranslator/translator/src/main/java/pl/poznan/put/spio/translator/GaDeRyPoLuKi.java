package pl.poznan.put.spio.translator;

import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/*
 * Translator podstawieniowy na bazie klucza 'gaderypoluki'
 * pl.wikipedia.org/wiki/Gaderypoluki
 */
// uwaga na adnotacje springa, to jest serwis
@Service
public class GaDeRyPoLuKi {

    private Map<String, String> map;

    /**
     * Mapa translacji, używany kod to: gaderypoluki. Poprawnie szyfrują się
     * tylko małe litery, nie wszystkie podlegają translacji.
     */
    public GaDeRyPoLuKi() {
        map = new HashMap<String, String>();
        map.put("g", "a");
        map.put("a", "g");
        map.put("d", "e");
        map.put("e", "d");
        map.put("r", "y");
        map.put("y", "r");
        map.put("p", "o");
        map.put("o", "p");
        map.put("l", "u");
        map.put("u", "l");
        map.put("k", "i");
        map.put("i", "k");
    }


    /**
     * Zwraca długość kodu szyfrującego, dla gaderypoluki to 12
     *
     * @return długość kodu
     */
    public int getCodeLength() {
        return map.size();
    }

    /**
     * Sprawdzenie czy dany znak należy do kodu szyfrującego.
     *
     * @param c znak do sprawdzenia
     * @return prawda, gdy znak należy do kodu
     */
    public boolean isTranslatable(String c) {
        return map.containsKey(c);
    }

    private Map<String, String> toMap(String key) {
        Map<String, String> map = new HashMap<>();
        for (int i = 0; i < key.length(); i += 2) {
            map.put(String.valueOf(key.charAt(i)), String.valueOf(key.charAt(i + 1)));
            map.put(String.valueOf(key.charAt(i + 1)), String.valueOf(key.charAt(i)));
        }
        return map;
    }

    /**
     * Translacja za pomocą wewnętrznego kodu, obsługiwane tylko małe znaki w wiadomości.
     *
     * @param msg wiadomość do translacji
     * @return zaszyfrowany napis
     */
    public String translate(String msg) {
        return translate(msg, this.map);
    }

    private String translate(String msg, Map<String, String> map) {
        String result = "";

        for (int i = 0; i < msg.length(); i++) {
            if (map.containsKey("" + msg.charAt(i))) {
                result += map.get("" + msg.charAt(i));
            } else {
                result += "" + msg.charAt(i);
            }
        }

        return result;
    }

    /**
     * Translacja za pomocą zadanego kodu.
     *
     * @param msg
     * @param key
     * @return
     */
    public String translate(String msg, String key) {
        return translate(msg, toMap(key));
    }

    /**
     * Translacja za pomocą kodu, wiadomość konwertowana na małe litery, co
     * powoduje że każdy napis powinien dać się przekonwertować.
     *
     * @param msg wiadomość do translacji
     * @return zaszyfrowany napis
     */
    public String translateIgnoreCase(String msg) {
        msg = msg.toLowerCase();
        return translate(msg, this.map);
    }

    public String translateIgnoreCase(String msg, String key) {
        msg = msg.toLowerCase();
        return translate(msg, toMap(key));
    }

}
